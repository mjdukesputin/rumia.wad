Git repository for my Doom episode, where I'll push out updates.

Instructions

- Download as a zip file
- Extract to a folder
- Install GZdoom for your platform
- Start GZdoom with your copy of Doom 2 and the PWAD.

Recent changes

- Finished the PWAD
- Replaced broken zscript extension for BossBrain with DECORATE (I'll go back and replace it with zscript as DECORATE is likely to become deprecated at some point, but I'm still unfamiliar with it.)
- Added a few remixed midis of classic DOOM music that I produced
- Added a secret level ;-)

Thanks to id Software for making a pretty cool game.